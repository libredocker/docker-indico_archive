FROM ubuntu:18.04


# Install packages
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --install-recommends \
    build-essential \
    libxslt1-dev libxml2-dev libffi-dev libpcre3-dev libyaml-dev \
    libjpeg-turbo8-dev zlib1g-dev liblz4-tool \
    libpq-dev \
    wget  \
    uwsgi uwsgi-plugin-python \
  	git \
  	python python-dev python-virtualenv python-setuptools python-pip \
  	#nginx \
  	#supervisor
    && rm -rf /var/lib/apt/lists/* \
    && pip install -U pip setuptools


# Install celery,uwsgi and cleanup the cache
RUN pip install uwsgi celery --cache-dir=/tmp/pip_cache && rm -rf /tmp/pip_cache


# Create user indico
RUN useradd -rm -d /opt/indico -s /bin/bash indico
USER indico


# Install indico
RUN virtualenv opt/indico/.venv && . opt/indico/.venv/bin/activate  && pip install indico==2.3


# Prepare folders
RUN mkdir /opt/indico/archive && \
    mkdir /opt/indico/assets && \
    mkdir /opt/indico/cache && \
    mkdir /opt/indico/etc && \
    mkdir /opt/indico/log && \
    mkdir /opt/indico/log/nginx && \
    mkdir /opt/indico/log/celery && \
    mkdir /opt/indico/tmp && \
    mkdir /opt/indico/web

# Copy files
COPY indico.wsgi /opt/indico/web/
COPY logging.yaml /opt/indico/logging.yaml
