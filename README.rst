Dockerized Indico
=================

This is a dockerized indico `Indico <https://docs.getindico.io>`_ instance.
Currently it uses the development version of the flask web server.

Preparation
-----------

- Clone the repository: ``git clone https://gitlab.com/artur-scholz/docker-indico.git``

- Enter the directory: ``cd docker-indico``

- Change the passwords in the files *indico.conf* and *docker-compose.yml* and
  adjust the file content to your server configuration.

- Prepare the postgres database of the container as follows:
    - Bring up the container: ``docker-compose up --build``
    - In a second terminal, find the ID of the running *postgres* container (has postgres in its name): ``docker ps``
    - Bash into this running postgres container using the ID: ``docker exec -it <ID> bash`` and run:
        - ``su - postgres -c 'createuser indico --pwprompt'``
        - ``su - postgres -c 'createdb -O indico indico'``
        - ``su - postgres -c 'psql indico -c "CREATE EXTENSION unaccent; CREATE EXTENSION pg_trgm;"'``
        - ``exit``
    - Stop the container from the first terminal (Crtl-C)

- Populate the database:
    - Bash into the container: ``docker-compose run indico bash``
    - Execute the following: ``source ~/.venv/bin/activate && indico db prepare``
    - Exit and stop the container: ``exit``

Running the Server
------------------

When preparation has been completed successfully, the container can be started
and stopped at any time.

When running for the first time, a bootstrap  will be done to create the superuser.

To start the container, issue: ``docker-compose up``. This runs it in interactive
mode with the log output printed on screen. Stop it with Ctr-C.

To run container in daemon mode, issue: ``docker-compose up -d`

The website is accessible at `http://localhost:80 <http://localhost:80>`_

Import/Export of Indico Events
------------------------------

To facilitate the migration of events from one server to another, indico provides
an easy to use utility function that dumps/loads the database content to/from
single files.

The docker-compose.yml file maps the folder *share* of this repository into the
container at /opt/share, to make it easy to transfer the files.

To import/export events:
- Bash into the indico container: ``docker-compose run indico bash`` and:
    - ``source /opt/indico/.venv/bin/activate``
    - ``cd /opt/share``
    - To import an event (eg. file *1.ind*): ``indico event import 1.ind``
    - To export an event (eg. file *1.ind*): ``indico event export 1 1.ind``
    - Exit and stop the container: ``exit``
